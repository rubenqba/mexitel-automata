package com.example.demo;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
public class SeleniumUtils {
    public static ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
            try {
                return ((Long) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active") == 0);
            } catch (Exception e) {
                // no jQuery present
                return true;
            }
        }
    };

    public static ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
            return ((JavascriptExecutor) webDriver).executeScript("return document.readyState")
                    .toString().equals("complete");
        }
    };

    public static void waitToLoad(WebDriver driver, int timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(jsLoad);
    }
}
