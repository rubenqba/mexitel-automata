package com.example.demo;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Service
@Order(1)
@Slf4j
public class MexitelDateCreator extends AbstractSeleniumExtractor {

    private static final String COUNTRY_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectPais_panel\"]//li[text()='%s']";
    private static final String DOCUMENT_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectTipoDocumento_panel\"]//li[text()='%s']";
    private static final String TRAMITE_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectTramite_panel\"]//li[text()='%s']";
    private static final String TIPO_TRAMITE_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectTipoTramite_panel\"]//li[text()='%s']";
    private static final String PASSPORT_SELECTOR = "formRegistroCitaExtranjero:noPasapAnt";
    private static final String PASSPORT_COUNTRY_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectPaisPasaporte_panel\"]//li[text()='%s']";;
    private static final String FIRSTNAME_SELECTOR = "formRegistroCitaExtranjero:nombre";
    private static final String LASTNAME_SELECTOR = "formRegistroCitaExtranjero:Apellidos";
    private static final String NATIONALITY_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectNacionalidad_panel\"]//li[text()='%s']";
    private static final String BIRTHDATE_SELECTOR = "formRegistroCitaExtranjero:fechaNacimiento_input";
    private static final String BIRTHCOUNTRY_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:selectPaisNacimiento_panel\"]//li[text()='%s']";
    private static final String SEX_SELECTOR = "//*[@id=\"formRegistroCitaExtranjero:sexo_panel\"]//li[text()='%s']";
    private static final String PHONE_SELECTOR = "formRegistroCitaExtranjero:telmovil";

    private final Random rand;
    private MexitelProperties config;
    private GoogleProperties google;
    private Sheets sheets;


    @Autowired
    public MexitelDateCreator(ApplicationContext ctx, MexitelProperties config, GoogleProperties google, Sheets sheets) {
        super(ctx);
        this.config = config;
        this.google = google;
        this.sheets = sheets;
        rand = new Random();
    }

    private List<ClientData> getRequests() {
        log.debug("recuperando lista de chequeo...");
        try {
            ValueRange response = sheets.spreadsheets().values()
                    .get(google.getDocument(), "clientes!A2:E")
                    .execute();

            List<List<Object>> values = response.getValues();
            if (values == null || values.size() == 0) {
                log.info("No data found.");
            } else {
                log.debug("hay elementos que buscar");
                return values.stream()
                        .map(row -> ClientData.builder()
                                .firtName((String) row.get(0))
                                .lastName((String) row.get(1))
                                .passport((String) row.get(2))
                                .birthDate((String) row.get(3))
                                .sex((String) row.get(4))
                                .build())
                        .collect(Collectors.toList());
            }

        } catch (IOException e) {
            log.error("{}", e);
        }
        return new ArrayList<>();
    }

    public void getTracks() {
        getRequests().stream().forEach(this::getTrack);
    }

    protected void getTrack(ClientData session)  {
        RemoteWebDriver driver = getDriver();

        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);

            driver.get(config.getStartUrl());
            wait.until(ExpectedConditions.elementToBeClickable(By.id("btnLoginII")));

            Credential auth = selectCredential();
            log.debug("using username '{}' and password '{}'", auth.getUsername(), auth.getPassword());
            log.debug("{}", session);
            driver.findElement(By.id("j_username")).clear();
            driver.findElement(By.id("j_username")).sendKeys(auth.getUsername());
            driver.findElement(By.id("j_password")).clear();
            driver.findElement(By.id("j_password")).sendKeys(auth.getPassword());
            driver.findElement(By.id("btnLoginII")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("formRegistroCitaExtranjero:selectPais_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectPais_label")).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id("formRegistroCitaExtranjero:selectPais_filter")));
            driver.findElement(By.xpath(String.format(COUNTRY_SELECTOR, session.getCountry()))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:selectTipoDocumento_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectTipoDocumento_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:selectTipoDocumento_panel")));
            driver.findElement(By.xpath(String.format(DOCUMENT_SELECTOR, session.getDocument()))).click();

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:selectTramite_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectTramite_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:selectTramite_panel")));
            driver.findElement(By.xpath(String.format(TRAMITE_SELECTOR, session.getTramite()))).click();

            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:selectTipoTramite_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectTipoTramite_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:selectTipoTramite_panel")));
            driver.findElement(By.xpath(String.format(TIPO_TRAMITE_SELECTOR, session.getTipoTramite()))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PASSPORT_SELECTOR)));
            driver.findElement(By.id(PASSPORT_SELECTOR)).clear();
            driver.findElement(By.id(PASSPORT_SELECTOR)).sendKeys(session.getPassport());
            driver.findElement(By.id(PASSPORT_SELECTOR)).sendKeys(Keys.TAB);

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:selectPaisPasaporte_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectPaisPasaporte_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:selectPaisPasaporte_panel")));
            driver.findElement(By.xpath(String.format(PASSPORT_COUNTRY_SELECTOR, session.getEmiterCountry()))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(FIRSTNAME_SELECTOR)));
            driver.findElement(By.id(FIRSTNAME_SELECTOR)).clear();
            driver.findElement(By.id(FIRSTNAME_SELECTOR)).sendKeys(session.getFirtName());
            driver.findElement(By.id(LASTNAME_SELECTOR)).sendKeys(Keys.TAB);

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(LASTNAME_SELECTOR)));
            driver.findElement(By.id(LASTNAME_SELECTOR)).clear();
            driver.findElement(By.id(LASTNAME_SELECTOR)).sendKeys(session.getLastName());
            driver.findElement(By.id(LASTNAME_SELECTOR)).sendKeys(Keys.TAB);

            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:selectNacionalidad_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectNacionalidad_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:selectNacionalidad_panel")));
            driver.findElement(By.xpath(String.format(NATIONALITY_SELECTOR, session.getNationality()))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(BIRTHDATE_SELECTOR)));
            driver.findElement(By.id(BIRTHDATE_SELECTOR)).clear();
            driver.findElement(By.id(BIRTHDATE_SELECTOR)).sendKeys(session.getBirthDate());
            driver.findElement(By.id(BIRTHDATE_SELECTOR)).sendKeys(Keys.TAB);

            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:selectPaisNacimiento_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:selectPaisNacimiento_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:selectPaisNacimiento_panel")));
            driver.findElement(By.xpath(String.format(BIRTHCOUNTRY_SELECTOR, session.getBirthCountry()))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("formRegistroCitaExtranjero:sexo_label")));
            driver.findElement(By.id("formRegistroCitaExtranjero:sexo_label")).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("formRegistroCitaExtranjero:sexo_panel")));
            driver.findElement(By.xpath(String.format(SEX_SELECTOR, session.getSex()))).click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(PHONE_SELECTOR)));
            driver.findElement(By.id(PHONE_SELECTOR)).clear();
            driver.findElement(By.id(PHONE_SELECTOR)).sendKeys(session.getMobilePhone());
            driver.findElement(By.id(PHONE_SELECTOR)).sendKeys(Keys.TAB);

            wait.until(ExpectedConditions.presenceOfElementLocated(By.className("ui-dialog")));
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ui-dialog")));

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("reCaptchaGoogle")));

            String currentWindows = driver.getWindowHandle();
            WebDriver capcha = driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='reCaptchaGoogle']//iframe")));

            capcha.findElement(By.xpath("//span[@role='checkbox']")).click();

//            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@name,'TripPlanBtn')]")));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            savePage(driver, "mexitel-" + LocalDateTime.now()
                    .format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")));
            screenShot(driver, "mexitel-" + LocalDateTime.now()
                    .format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss")));
            throw e;
//        } finally {
//            driver.quit();
        }
    }

    private Credential selectCredential() {
        return config.getCredentials().get(rand.nextInt(config.getCredentials().size()));
    }
}
