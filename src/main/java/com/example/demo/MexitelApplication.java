package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MexitelApplication {

    @Autowired
    private MexitelDateCreator app;

    public static void main(String[] args) {
        SpringApplication.run(MexitelApplication.class, args);
    }

    @Bean
    public CommandLineRunner runAt() {
        return args -> {
            app.getTracks();
        };
    }
}
