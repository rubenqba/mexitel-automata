package com.example.demo;

import lombok.Data;

import java.util.List;

@Data
public class MexitelProperties {
    private String startUrl;
    private List<Credential> credentials;
}

@Data
class Credential {
    private String username;
    private String password;
    private boolean enabled;
}
