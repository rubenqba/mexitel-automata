package com.example.demo;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Slf4j
public abstract class AbstractSeleniumExtractor {

    private ApplicationContext ctx;

    public AbstractSeleniumExtractor(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    protected void screenShot(RemoteWebDriver driver, String fn) {
        byte[] jpg = driver.getScreenshotAs(OutputType.BYTES);

        File f = new File(fn + ".jpg");
        try (FileOutputStream fos = new FileOutputStream(f)) {
            fos.write(jpg);
        } catch (IOException e) {
            log.error("{}", e);
        }
    }

    protected void savePage(RemoteWebDriver driver, String fn) {
        String xml = prettyXML(driver.getPageSource());
        try {
            Files.write(Paths.get(fn + ".html"), xml.getBytes());
        } catch (IOException e) {
            log.error("{}", e);
        }
    }

    protected String prettyXML(String xml) {
        try {
            Document doc = Jsoup.parse(xml);
            doc.outputSettings().indentAmount(4);
            return doc.toString();
        } catch (Exception e) {
            log.error("Error formateando XML", e);
            return xml;
        }
    }

    protected RemoteWebDriver getDriver() {
        return ctx.getBean(RemoteWebDriver.class);
    }

}
