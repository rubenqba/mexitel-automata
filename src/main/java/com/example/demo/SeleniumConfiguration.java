package com.example.demo;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.env.Environment;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ Idea
 *
 * @author ruben.bresler
 */
@Configuration
//@Log4j
@Log
public class SeleniumConfiguration {

    @Bean(destroyMethod = "quit")
    @Scope("prototype")
    public RemoteWebDriver createDriver(Environment env) {
        String driver = env.getRequiredProperty("monitor.driver");

        log.info("usando driver: " + driver);
        RemoteWebDriver webDriver;
        if ("chrome".equalsIgnoreCase(driver)) {
            webDriver = createChromeDriver(env);
        } else if ("firefox".equalsIgnoreCase(driver)) {
            webDriver = createFirefoxDriver(env);
        } else if ("phantomjs".equalsIgnoreCase(driver)) {
            webDriver = createPhantomJs(env);
        } else {
            throw new IllegalArgumentException("driver incorrecto ['chrome', 'firefox', 'phantomjs']");
        }

        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        webDriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        webDriver.manage().window().setSize(new Dimension(1024, 768));

        return webDriver;
    }

    @Bean
    @ConfigurationPropertiesBinding
    public Converter<String, LocalDate> dateConverter() {
        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String s) {
                return StringUtils.isNotBlank(s) ? LocalDate.parse(s, DateTimeFormatter.ISO_DATE) : null;
            }
        };
    }

    private RemoteWebDriver createChromeDriver(Environment env) {
        ChromeOptions options = new ChromeOptions();
        if (StringUtils.isNotBlank(env.getProperty("webdriver.path.chrome"))) {
            System.setProperty("webdriver.chrome.driver", env.getProperty("webdriver.path.chrome"));
        }
        return new ChromeDriver(options);
    }


    private RemoteWebDriver createPhantomJs(Environment env) {
        DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
        if (StringUtils.isNotBlank(env.getProperty("webdriver.path.phantomjs"))) {
            capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                    env.getProperty("webdriver.path.phantomjs"));
        }

        return new PhantomJSDriver(capabilities);
    }

    private RemoteWebDriver createFirefoxDriver(Environment env) {
        FirefoxOptions options = new FirefoxOptions();
//        FirefoxBinary binary = new FirefoxBinary(new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe"));
//        options.setBinary(binary);
        if (StringUtils.isNotBlank(env.getProperty("webdriver.path.gecko"))) {
            System.setProperty("webdriver.gecko.driver", env.getProperty("webdriver.path.gecko"));
        }
        return new FirefoxDriver(options);
    }

    @Bean
    @ConfigurationProperties(prefix = "google")
    public GoogleProperties createGoogleConfig() {
        return new GoogleProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "mexitel")
    public MexitelProperties createMexitelConfig() {
        return new MexitelProperties();
    }
}
