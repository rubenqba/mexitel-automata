package com.example.demo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientData {
    private String url;
    private String username;
    private String password;

    @Builder.Default
    private String country = "CUBA";

    @Builder.Default
    private String document = "VISAS";

    @Builder.Default
    private String tramite = "SIN PERMISO DEL INM";

    @Builder.Default
    private String tipoTramite = "TURISMO Y TRANSITO";
    private String passport;

    @Builder.Default
    private String emiterCountry = "CUBA";
    private String firtName;
    private String lastName;

    @Builder.Default
    private String nationality = "CUBANA";
    private String birthDate;

    @Builder.Default
    private String birthCountry = "CUBA";
    private String sex;

    @Builder.Default
    private String mobilePhone = "512104977";
}
