package com.example.demo;

import lombok.Data;

@Data
public class GoogleProperties {

    private String applicationName;
    private String document;
}
